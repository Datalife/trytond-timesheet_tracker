datalife_timesheet_tracker
==========================

The timesheet_tracker module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-timesheet_tracker/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-timesheet_tracker)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
